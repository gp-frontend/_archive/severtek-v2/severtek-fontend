
var mobileMenu = $('.mobile-menu'),
    buttonMenuOpen = $('.js-menu-button-open'),
    buttonMenuClose = $('.js-menu-button-close'),
    linkSubmenuOpen = $('.js-link-submenu-open'),
    linkSubmenuClose = $('.js-link-submenu-close');

// create overlay
$('body').append("<div class='mobile-menu-overlay'></div>");
$('body').append("<div class='mobile-menu-swipe-panel' data-menu='mobile-menu'></div>");

// open menu
function openMobileMenu (menuName) {
  $(menuName).toggleClass("mobile-menu_open");
  $('body').toggleClass('mobile-menu-opened');
  $('.mobile-menu-overlay').toggleClass('mobile-menu-overlay_visible');
}

// close menu
function closeMobileMenu () {
  $(mobileMenu).removeClass("mobile-menu_open");
  $('body').removeClass('mobile-menu-opened');
  $('.mobile-menu-overlay').removeClass('mobile-menu-overlay_visible');
}

// open submenu
function openMobileSubMenu ($this) {
  $this.siblings('.mobile-menu__submenu').addClass("mobile-menu__submenu_open");
}

// close submenu
function closeMobileSubMenu ($this) {
  $this.parent('.mobile-menu__submenu').removeClass("mobile-menu__submenu_open");
}

// click on button open
buttonMenuOpen.on('click', function (e) {
  e.preventDefault();

  var currentMenu = $(this).attr('data-menu');

  openMobileMenu('#' + currentMenu);
});

// click on button close
buttonMenuClose.on('click', function (e) {
  e.preventDefault();

  closeMobileMenu();
});

// click on overlay
$('.mobile-menu-overlay').on('click', function () {
  closeMobileMenu();
});

// click on link submenu
linkSubmenuOpen.on('click', function (e) {
  e.preventDefault();

  openMobileSubMenu($(this));
});

// click on back link submenu
linkSubmenuClose.on('click', function (e) {
  e.preventDefault();

  closeMobileSubMenu($(this));
});

// SWIPE EVENTS
$('.mobile-menu-swipe-panel').swipe({

  swipeLeft: function () {
    var currentMenu = $(this).attr('data-menu');
    openMobileMenu('#' + currentMenu);
  }
});

mobileMenu.swipe({
  swipeRight: function () {
    closeMobileMenu();
  }
});