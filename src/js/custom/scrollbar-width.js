// WIDTH OF SCROLLBAR
var widthOfScrollbar;

function getScrollBarWidth() {
  if (window.innerWidth > $(window).width()) {
    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
      widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
    $outer.remove();
    widthOfScrollbar = 100 - widthWithScroll;
    return 100 - widthWithScroll;
  } else {
    return widthOfScrollbar = 0;
  }
};

getScrollBarWidth();

$(window).on('resize', function() {
  getScrollBarWidth();
});

function addScrollbarCompensation(element) {
  element.css('padding-right', widthOfScrollbar);
}

function removeScrollbarCompensation(element) {
  element.css('padding-right', 0);
}

// example
addScrollbarCompensation($('.popup'));

removeScrollbarCompensation($('.popup'));